define(function(require, exports, module) {

    //Load Brackets modules
    var CommandManager = brackets.getModule("command/CommandManager"),
    Menus = brackets.getModule("command/Menus"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    DefaultDialogs = brackets.getModule("widgets/DefaultDialogs"),
    Document = brackets.getModule("document/Document"),
    FileSystem = brackets.getModule("filesystem/FileSystem"),
    DocumentManager = brackets.getModule("document/DocumentManager"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    WorkspaceManager = brackets.getModule("view/WorkspaceManager"),
    ExtensionUtils = brackets.getModule("utils/ExtensionUtils"),
    KeyBindingManager = brackets.getModule("command/KeyBindingManager"),
    CodeInspection = brackets.getModule("language/CodeInspection"),
    ExtensionLoader = brackets.getModule("utils/ExtensionLoader"),
    AppInit = brackets.getModule("utils/AppInit");

    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    var fontcollection = require("src/utils/fontcollection");
    var publicapi = require("src/utils/publicapi");
    
    var TYPOGRAPHY_EXECUTE = "streamliner.typography.execute";
    var typography_dialog = require("text!html/typography.html");
    var typographyDialog;
    
    var userDataPath;
    
    //Google Fonts
    var API_KEY = "AIzaSyBdBsTDu5OF9mjuU14rdjfwF6rVpj2rYpE";
    
    var fontItems;
    
    //Array of fonts added
    var fontsAdded = {};
    
    //Fonts loaded for comparison
    var fontsLoaded = [];
    
    //Enumeration for Page Ids
    var Page = {
        CAT_PAGE       : "#typo-page-fontcatselect",
        LIST_PAGE       : "#typo-page-fonts",
        BROWSE_PAGE : "#typo-page-browse",
        PERSONAL_PAGE : "#typo-page-personal"
    };
    
    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    /**
     * Populate fonts from data from the Google Fonts API.
     */
    function _populateFontsData()
    {
        //Clear previously loaded fonts
        fontsLoaded.length = 0;
        
        //Saving a new file
        var extPath = ExtensionLoader.getUserExtensionPath();
        userDataPath = extPath+"/"+utils.getExtensionDirectory()+"/UserData/Collections.txt";    
        
        /*
            Data gotten from the API
            data> kind, items
            items> #, length
            items[#]> kind, family, category, variants, subsets, version, lastModified, files
            family: name of the font
            category: serif, sans-serif
            variants: array [regular, italic, etc.]
            files: array[reguar: .ttf file, italif: .ttf file]
        */
        
        //Calling the Google Fonts API
        //Sorting by popularity add: sort=popularity
        $.get( "https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key="+API_KEY, function( data ) {
          //Object.getOwnPropertyNames(obj);
          utils.log("Got "+data.items.length+" fonts");
            fontItems = data;
            _toggleTypographyDialogWindow();
              
        }).fail(function(){
            //Show a dialog window with an error/failed state
            _toggleTypographyErrorDialogWindow();
        });
    }
    
    /**
     * Toggle typography window.
     */
    function _toggleTypographyDialogWindow()
    {
        
        //Clean previous session
        fontsAdded = {};
        
        typographyDialog = Dialogs.showModalDialogUsingTemplate(typography_dialog, true);
        
        $actionBtn = typographyDialog.getElement().find("#action-btn");
        
        
        // Upon closing the dialog, run function to gather and apply choices
        $actionBtn.on("click", _insertFonts);
        
        //Selected a category from the Category Page
        $typoBlock = typographyDialog.getElement().find(".typo-block");
        $typoBlock.on("click", function(){
            $categorySelected = $(this).attr("data-category");
            _populateCategoryListPage($categorySelected);
            _showPage(Page.LIST_PAGE);
        });
        
        $homeLink = typographyDialog.getElement().find("#navigate-home");
        $homeLink.on("click", function(){
            _showPage(Page.CAT_PAGE);
            $(".selected").removeClass("selected");
            $("#navigate-home").addClass("selected");
        });
        
        $browseLink = typographyDialog.getElement().find("#navigate-browse");
        $browseLink.on("click", function(){
            _populateBrowsePage();
            _showPage(Page.BROWSE_PAGE);
            $(".selected").removeClass("selected");
            $("#navigate-browse").addClass("selected");
        });
        
        $personalLink = typographyDialog.getElement().find("#navigate-personal");
        $personalLink.on("click", function(){
            _populatePersonalPage();
            _showPage(Page.PERSONAL_PAGE);
            $(".selected").removeClass("selected");
            $("#navigate-personal").addClass("selected");
        });
    }
    
    /**
     * Populate the browse page from the API.
     */
    function _populateBrowsePage()
    {
        publicapi.getLatestCollections(_populateBrowseCallback);
    }

    /**
     * Callback function that gets called when the API finishes returning fonts.
     */
    function _populateBrowseCallback(data)
    {
        //Get all fonts into an array for loading
        var allFonts = {};
        for (var i = 0 ;i<data.items.length; i++)
        {
            for (var j=0; j<data.items[i].fonts.length; j++)
            {
                if (allFonts[data.items[i].fonts[j].family] == undefined){
                    allFonts[data.items[i].fonts[j].family] = data.items[i].fonts[j].url;
                }
            }
        }
    
        Object.keys(allFonts).forEach(function(key, url){
            $typoModalContent = typographyDialog.getElement().find("#typo-modal-content");
            $typoModalContent.prepend("<link href='"+ allFonts[key] + "' rel='stylesheet' type='text/css'>");
            
            setTimeout(function(){ 
               $typoModalContent.prepend("<style>.typo-fontblock h4.typo-fontloading{display:none} .typo-fontblock h4{display: block}</style>");   
                $typoModalContent.prepend("<style>.typo-fontblock h4{display: block}</style>");
            }, 3000);
            
        });
    
        $typoBrowse = typographyDialog.getElement().find("#typo-font-list-browse");
        
        for(var i = 0 ; i<data.items.length ; i++)
        {
            var collectionFonts = data.items[i].fonts;
            var collectionDate = data.items[i].DateCreated;
            
            var listElement = [];
            
            listElement.push('<div class="typo-collection-block">');
            listElement.push('Date: '+collectionDate);
            
            for (var j = 0 ; j < collectionFonts.length; j++)
            {
                listElement.push('<li>');
                listElement.push('<div class="typo-fontblock">');
                    listElement.push('<h4 class="typo-fontloading"><i class="fa fa-spinner fa-spin"></i></h4>');
                    listElement.push('<h4 class="'+collectionFonts[j].family+'" style="font-family:\''+collectionFonts[j].family.split('+').join(' ')+'\'">The quick brown fox jumps over the lazy dog</h4>');
                    listElement.push('<h5>'+collectionFonts[j].family.split('+').join(' ')+'</h5>');
                    listElement.push('<a href="" data-item-top="'+i+'" data-item-bot="'+j+'" class="fontSelected font-add stream-add" id="col-browse'+i+'"><i class="fa fa-plus-circle"></i></a>');
                listElement.push('</div>');
                listElement.push('</li>');
                
            }
            
            listElement.push('</div');
            var fontBlockElement = listElement.join('');
            $typoBrowse.append(fontBlockElement);
        }
        
        $(".fontSelected").on("click", function(){
            //var currentFontItem = fontList[$(this).attr("data-item")];
            var currentFontItem = data.items[$(this).attr("data-item-top")].fonts[$(this).attr("data-item-bot")];
            var urlString = currentFontItem.url;
            if ($(this).hasClass("font-add"))
            {
                fontsAdded[currentFontItem.family] = urlString;

                $(this).removeClass("font-add");
                $(this).addClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
            else
            {
                delete fontsAdded[currentFontItem.family];
                
                //Remove
                $(this).addClass("font-add");
                $(this).removeClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            } 
        });   
    }
    
    /**
     * Populate the personal page from the local file.
     */
    function _populatePersonalPage()
    {
        fontcollection.readCollections(userDataPath, _populatePersonalPageCallback);
    }
    
    /**
     * Callback called when the personal fonts are loaded.
     */
    function _populatePersonalPageCallback(collectionsArray) {
        
        _loadFontsForCollection(collectionsArray);
        
        $typoPersonal = typographyDialog.getElement().find("#typo-font-list-personal");
        for(var i = collectionsArray.length-1 ; i>=0 ; i--)
        {
            var collectionFonts = collectionsArray[i].split(",");
            var collectionDate = collectionFonts[collectionFonts.length-1];
            
            var listElement = [];
            listElement.push('<div class="typo-collection-block">');
            
            listElement.push('Date: '+collectionDate);
            
            collectionFonts.pop();
            for (var j = 0 ; j < collectionFonts.length; j++)
            {
                listElement.push('<li>');
                listElement.push('<div class="typo-fontblock">');
                    listElement.push('<h4 class="typo-fontloading"><i class="fa fa-spinner fa-spin"></i></h4>');
                    listElement.push('<h4 class="'+_getFamilyByLink(collectionFonts[j])+'" style="font-family:\''+_getFamilyByLink(collectionFonts[j]).split('+').join(' ')+'\'">The quick brown fox jumps over the lazy dog</h4>');
                //utils.log("------Font-Family is : " + _getFamilyByLink(collectionFonts[j]).split('+').join(' '));
                    listElement.push('<h5>'+_getFamilyByLink(collectionFonts[j]).split('+').join(' ')+'</h5>');
                    listElement.push('<a href="" data-item-top="'+i+'" data-item-bot="'+j+'" class="fontSelected font-add stream-add" id="col-browse'+i+'"><i class="fa fa-plus-circle"></i></a>');
                listElement.push('</div>');
                listElement.push('</li>');
                
            }
            listElement.push('</div');
            var fontBlockElement = listElement.join('');
            $typoPersonal.append(fontBlockElement);
        }
        
        $(".fontSelected").on("click", function(){
            
            var collectionFonts = collectionsArray[$(this).attr("data-item-top")].split(",");
            var currentFontItem = _getFamilyByLink(collectionFonts[$(this).attr("data-item-bot")]);
            var urlString = collectionFonts[$(this).attr("data-item-bot")];
            if ($(this).hasClass("font-add"))
            {
                fontsAdded[currentFontItem] = urlString;

                $(this).removeClass("font-add");
                $(this).addClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
            else
            {
                delete fontsAdded[currentFontItem];
                
                //Remove
                $(this).addClass("font-add");
                $(this).removeClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
            
        });
    }
    
    /**
     * Gets the font-family by link
     * @param {string} link - Link to get the font-family from.
     */
    function _getFamilyByLink(link)
    {
        return link.substring(link.lastIndexOf('=')+1, link.lastIndexOf(':'));
    }
    
    /**
     * Get all font families by a category
     * @param {string} category - Category to look for e.g. serif
     */
    function _getAllFontFamiliesByCategory(category)
    {
        var allFonts = _getFontsByCategory(fontItems, category)
        
        var fontFamilies = [];
        allFonts.forEach(function(item){
            fontFamilies.push(item.family);
        });
        return fontFamilies;
    }
    
    /**
     * Load fonts by category
     * @param {string} category - Category to look for e.g. serif
     */
    function _loadFonts(category)
    {
        var allFonts = _getFontsByCategory(fontItems, category);
        //If I want just a section of the array
        //allFonts = allFonts.slice(0,50);
        
        _loadFontsByArray(allFonts);
        
    }
    
    /**
     * Load fonts for a particular collection
     * @param {string[]} collectionsArray - Array with collection information
     */
    function _loadFontsForCollection(collectionsArray)
    {
        var allFonts = [];
        for(var i = 0 ; i<collectionsArray.length ; i++)
        {
            var collectionFonts = collectionsArray[i].split(",");
            
            //Get rid of the date
            collectionFonts.pop();
            
            for (var j = 0 ; j < collectionFonts.length; j++)
            {
                var currentFont = collectionFonts[j];
                
                //Only add the font if it is not already on the array
                if (allFonts.indexOf(currentFont) == -1)
                {
                    allFonts.push(currentFont);
                }
            }
        }
        
        //Find a way to load fonts
        _loadFontsFromURLArray(allFonts);
    }
    
    /**
     * Load fonts from array or url
     * @param {string[]} allFonts
     */
    function _loadFontsFromURLArray(allFonts)
    {
        allFonts.forEach(function(current){       
            if (fontsLoaded.indexOf(current.split(" ").join()) == -1) {
                $typoModalContent = typographyDialog.getElement().find("#typo-modal-content");
                $typoModalContent.prepend("<link href='"+current+"' rel='stylesheet' type='text/css'>");

                fontsLoaded.push(current.split(" ").join());
            }
        });
        
        typographyDialog.getElement().find(".typo-fontloading").hide();
                $typoModalContent.prepend("<style>.typo-fontblock h4.typo-fontloading{display:none} .typo-fontblock h4{display: block}</style>");
    }
    
    /**
     * Load fonts by array.
     * @param {string[]} allFonts
     */
    function _loadFontsByArray(allFonts)
    {
        allFonts.forEach(function(current){
            if (fontsLoaded.indexOf(current.family.split(" ").join()) == -1) {
                $typoModalContent = typographyDialog.getElement().find("#typo-modal-content");
                $typoModalContent.prepend("<link href='"+_buildURL(current)+"' rel='stylesheet' type='text/css'>");
                
                setTimeout(function(){ 
                   $typoModalContent.prepend("<style>.typo-fontblock h4.typo-fontloading{display:none}.typo-fontblock ."+current.family.replace(/ /g, '')+"{display: block}</style>");   
                }, 3000);   

                fontsLoaded.push(current.family.split(" ").join());
            }
        }); 
    }
    
    /**
     * Insert fonts into the editor.
     */
    function _insertFonts()
    {
        //Check how the user wants to insert the fonts (html or css)
        $selectedOption = typographyDialog.getElement().find("input:radio[name ='typo-dloption']:checked");
        
        //Insert the CSS Reset Code into the current document
        var editor = EditorManager.getActiveEditor();
        
        if (editor) {
            //Save the collection for the users records
            if (Object.keys(fontsAdded).length > 0)
            {
                fontcollection.writeCollection(userDataPath, fontsAdded);
                if (utils.isFontShared()) {
                    publicapi.addCollection(fontsAdded);
                }   
            }
          
            // Get the cursor position
            var cursor = editor.getCursorPos();

            editor.document.batchOperation(function() {
            // Insert the selected elements at the current cursor position
                Object.keys(fontsAdded).forEach(function(key, url){
                    if ($selectedOption.val() == "css")
                    {
                        editor.document.replaceRange("@import url("+fontsAdded[key]+");" + "\n", cursor);
                    }
                    else
                    {
                        editor.document.replaceRange("<link href='"+fontsAdded[key]+"' rel='stylesheet' type='text/css'>" + "\n", cursor);
                    }
                });
            });                       
        }
        typographyDialog.close();   
    }
    
    /**
     * Populate the category list page given a category
     * @param {string} category - Category to display e.g. serif
     */
    function _populateCategoryListPage(category)
    {
        _loadFonts(category);
        
        typographyDialog.getElement().find(".typo-category-selected").html(category);
        $catFontList = typographyDialog.getElement().find("#typo-font-list");
        
        //Clear list
        $catFontList.html("");
        
        var fontList = _getFontsByCategory(fontItems, category);
        utils.log("For the category "+ category +", got: "+fontList.length);
        
        for(var i=0; i<fontList.length; i++)
        {   
            //Building Block inside the list        
            var listElement = [];
            listElement.push('<li>');
                listElement.push('<div class="typo-fontblock">');
                    listElement.push('<h4 class="typo-fontloading"><i class="fa fa-spinner fa-spin"></i></h4>');
                    listElement.push('<h4 class="'+fontList[i].family.replace(/ /g, '')+'" style="font-family:\''+fontList[i].family+'\'">The quick brown fox jumps over the lazy dog</h4>');
                    listElement.push('<h5>'+fontList[i].family+'</h5>');
                    listElement.push('<a href="" data-item="'+i+'" class="fontSelected font-add stream-add"><i class="fa fa-plus-circle"></i></a>');
                listElement.push('</div>');
            listElement.push('</li>');
            
            
            var fontBlockElement = listElement.join('');
            
            $catFontList.append(fontBlockElement);     
        }
        
        $(".fontSelected").on("click", function(){
            var currentFontItem = fontList[$(this).attr("data-item")];
            var urlString = _buildURL(currentFontItem);
            if ($(this).hasClass("font-add"))
            {
                fontsAdded[currentFontItem.family] = urlString;

                $(this).removeClass("font-add");
                $(this).addClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
            else
            {
                delete fontsAdded[currentFontItem.family];
                
                //Remove
                $(this).addClass("font-add");
                $(this).removeClass("font-remove");
                $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
        });
    }
    
    /**
     * Verifies if an array contains an item
     * @param {string} item - Item to search for
     * @param {string[]} arrayOfItems - Array to search in.
     */
    function contains(item, arrayOfItems)
    {
        if (arrayOfItems.indexOf(item) >=0 ) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Build url 
     * @param {string} fontItem
     */
    function _buildURL(fontItem)
    {
        var apiUrl = [];
        apiUrl.push('http://fonts.googleapis.com/css?family=');
        apiUrl.push(fontItem.family.replace(/ /g, '+'));
        
        if (contains('400', fontItem.variants)) {
          apiUrl.push(':');
          apiUrl.push('400');
        }
        else
        {
            apiUrl.push(':');
            apiUrl.push(fontItem.variants[0]);
        }
        
        var url = apiUrl.join('');
        return url;
    }
    
    /**
     * Show a particular page
     * @param {string} pageId - The HTML id of the page to display.
     */
    function _showPage(pageId)
    {
        //Hide all the pages
        $.each(Page , function(key, value) { 
          $(value).fadeOut(600, function(){
            //Show only the desired page
            $(pageId).show();
          });
        });
    }
    
    /**
     * Toggles error window.
     */
    function _toggleTypographyErrorDialogWindow()
    {
        utils.log("Errors Dialog Window");
    }
    
    /**
     * Get fonts by category
     * @param {json} data - json object with the data of all fonts
     * @param {string} category - Category to load e.g. serif or sans-serif
     */
    function _getFontsByCategory(data, category)
    {
        var fontList = [];
        if (category)
        {
            for (var i=0; i<data.items.length; i++)
            {
                if (data.items[i].category == category)
                {
                    fontList.push(data.items[i]);
                }
            }
        }
        else  //get every font if the category is empty
        {
            for (var i=0; i<data.items.length; i++)
            {
                fontList.push(data.items[i]);
            }
        }

        if (fontList.length>20)
        {
            fontList.splice(0, 20);
        }
        
        return fontList;
    }
    
    /**
     * Get all categories
     * @param {json} data - json object with the data of all fonts
     */
    function _getAllCategories(data)
    {
        var categories = [];
        for (var i=0; i<data.items.length; i++)
        {
            if (categories.indexOf(data.items[i].category)<0)
            {
                categories.push(data.items[i].category);
            }
        }
        return categories;
    }
    
    /**
     * Register this functionality into the App.
     */
    function _registerCommand()
    {
        CommandManager.register("Typography Tool...", TYPOGRAPHY_EXECUTE, _populateFontsData);
        global.menu.addMenuItem(TYPOGRAPHY_EXECUTE, "Ctrl-Shift-T");
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };

});