/**
 * Controls the CSS portion of the Code Hinter
 */
define(function (require, exports, module) {
    "use strict";

    var AppInit             = brackets.getModule("utils/AppInit"),
        CodeHintManager     = brackets.getModule("editor/CodeHintManager"),
        DocumentManager     = brackets.getModule("document/DocumentManager"),
        LanguageManager     = brackets.getModule("language/LanguageManager"),
        CommandManager      = brackets.getModule("command/CommandManager"),
        Menus               = brackets.getModule("command/Menus"),
        Launcher       = brackets.getModule("LiveDevelopment/MultiBrowserImpl/launchers/Launcher"),
        Dialogs             = brackets.getModule("widgets/Dialogs"),
        DefaultDialogs      = brackets.getModule("widgets/DefaultDialogs"),
        WorkspaceManager    = brackets.getModule("view/WorkspaceManager"),
        ExtensionUtils      = brackets.getModule("utils/ExtensionUtils"),
        ExtensionLoader     = brackets.getModule("utils/ExtensionLoader"),
        KeyBindingManager   = brackets.getModule("command/KeyBindingManager");
    
    
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    var documentationHandler = require("src/utils/documentationhandler");
    var savedlinks = require("src/utils/savedlinks");
    
    var CSS_CODE_HINT_EXECUTE = "streamliner.csscodehint.execute";
    var panelCSS = require("text!html/codehintpanel.html");
    var panel;
    var HINT_PANEL = "streamliner.bottompanel.codehintcss";
    var documentable = require("src/utils/documentable");
    
    
    var documentableElementsArray;
    
    var documentationLoaded = false;
    
    var mdnPageContents;
    var W3CPageContents;
    var W3SchoolsPageContents;
    
    var bookmarkElement;
    var loadedKeyword;
    var savedLinksList;
    var bookmarkArrayInstance;
    
    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str){
        return this.slice(0, str.length) == str;
      };
    }

    /**
     * Gets the CSS element that matches what is written
     * @param {string} written - What is currently written.
     */
    function _getMatchingElements(written)
    {
        var matchArray = [];
        
        if (written == null || written.length == 0)
            return matchArray;
        
        for (var i=0; i<documentableElementsArray.length; i++)
        {
            var currentElement = documentableElementsArray[i];
            if (currentElement.startsWith(written))
            {
                matchArray.push(documentableElementsArray[i]);
            }
        }
        return matchArray;
    }
    
    /**
     * Displays that no documentation has been found
     * @param {string} td - Container where the message will be displayed.
     */
    function _displayNoDocumentationMessage(td)
    {
        //Clear the contents
        panel.$panel.find(td).html("");
        
        //Show the no documentation message on the given td
        //panel.$panel.find(td + " " + ".invisible").removeClass("invisible");
        panel.$panel.find(td).html('<div class="alert alert-danger" role="alert">There is no documentation from this source</div>');
    }
    
    /**
     * Loads the code hinter documentation.
     * @param {string} element - The CSS element to search for.
     */
    function _getCodeHintForElement(element)
    {
        _clearListOfMatches();
        documentationLoaded = true;
        loadedKeyword = element;
        if (savedLinksList.children("#bookmarkelem-"+loadedKeyword).length > 0) {
            _markBookmark();
        } else {
            _unmarkBookmark();
        }
        bookmarkElement.show();
        
        //documentationLoaded = true;
        _clearListOfMatches();
        
        //Get code hint from the different sources
        var mdnURL = documentationHandler.createMDNCSSURL(element);
        panel.$panel.find("#hint-mdn").load(mdnURL + " #wikiArticle", function(response, status, xhr){
            if (status == "error")
            {
                _displayNoDocumentationMessage("#hint-mdn");
            }
        });
        
        
        var url1success = true;
        var w3SchoolsURL = documentationHandler.createW3SchoolsCSSURL1(element);
        panel.$panel.find("#hint-schools").load(w3SchoolsURL + " .main",
                                                function( response, status, xhr ){
                                                    if (status == "error"){
                                                        var w3SchoolsURL = documentationHandler.createW3SchoolsCSSURL2(element);
                                                        panel.$panel.find("#hint-schools").load(w3SchoolsURL + " .main",
                                                                                            function( response, status, xhr ){
                                                                                                if (status == "error"){
                                                                                                    //Display message in panel that there is no documentation for this site
                                                                                                    _displayNoDocumentationMessage("#hint-schools");
                                                                                                }
                                                                                            });
                                                    };    
                                                });
        
    }

    
    /**
     * @constructor
     */
    function ECDHint() {
        
    }
    
    /**
     * Hides the CSS Code Hint panel.
     */
    function _hidePanel()
    {
        panel.hide();
        CommandManager.get(CSS_CODE_HINT_EXECUTE).setChecked(false);
    }

    /**
     * Toggles the CSS Code Hint panel.
     */
    function _togglePanel()
    {
        if (panel.isVisible())
        {
            _hidePanel();
        }
        else
        {
            panel.show();
            CommandManager.get(CSS_CODE_HINT_EXECUTE).setChecked(true);
        }
    }
    
    /**
     * Clear list of current matches.
     */
    function _clearListOfMatches()
    {
        panel.$panel.find("#list-of-matches").html("");
    }
 
    /**
     * Checks, if it is possible to give hints.
     */
    ECDHint.prototype.hasHints = function (editor, implicitChar) {
        this.editor = editor;
        
        return true;
        
    };

    /**
     * Calculates the hints
     */
    ECDHint.prototype.getHints = function (implicitChar) {
        
        _clearListOfMatches();
        
        var hintArray = [];
        
        var cursor = this.editor.getCursorPos();
		var lineBeginning = {line:cursor.line,ch:0};
		var textBeforeCursor = this.editor.document.getRange(lineBeginning, cursor);
		textBeforeCursor=textBeforeCursor.split(' ')
		textBeforeCursor=textBeforeCursor[textBeforeCursor.length-1];
        
        panel.$panel.find("#being-searched").html(textBeforeCursor);
        
        
        var matchedArray = _getMatchingElements(textBeforeCursor);
        
        //Decide to wether show the list of matching elements, or just show
        //the documentation if there is just one matched element
        if (matchedArray.length == 1 && !documentationLoaded)
        {
            _getCodeHintForElement(matchedArray[0]);
        }
        else
        {
            for (var i=0; i<matchedArray.length ; i++)
            {
                panel.$panel.find("#list-of-matches").append("<li><a href='#' class='hint-to-search'>"+matchedArray[i]+"</a></li>");
            }
        }
        
        panel.$panel.find(".hint-to-search").on("click", function(){
            var hintToSearch = $(this).html();
            _getCodeHintForElement(hintToSearch);
        });
        
        return null;
    };
    
    /**
     * Inserts the hint
     */
    ECDHint.prototype.insertHint = function (hint) {
        
        // Get the color from hint
        var code = hint;
        
        // Document objects represent file contents
        var currentDoc = DocumentManager.getCurrentDocument();
        
        // Get the position of our cursor in the document
        var pos = this.editor.getCursorPos();
        
        // Add some text in our document
        currentDoc.replaceRange(code, pos);
        
    };
    
    /**
     * Save a CSS keywork bookmark.
     */
    function _saveCSSKeyword(keyword) {
        var extPath = ExtensionLoader.getUserExtensionPath();
        var userDataPath = extPath+"/"+utils.getExtensionDirectory()+"/UserData/CSSKeywords.txt";
        savedlinks.saveSource(userDataPath, keyword);
    }
    
    /**
     * Changes the Bookmark toggle CSS to reflect that the element is bookmarked.
     */
    function _markBookmark()
    {
        bookmarkElement.find("i").removeClass("fa-bookmark-o");
        bookmarkElement.find("i").addClass("fa-bookmark");
    }
    
    /**
     * Changes the Bookmark toggle CSS to reflect that the element not is bookmarked.
     */
    function _unmarkBookmark()
    {
        bookmarkElement.find("i").removeClass("fa-bookmark");
        bookmarkElement.find("i").addClass("fa-bookmark-o");
    }
    
    /**
     * Load existing bookmarks for the user.
     */
    function _loadBookmarkedList()
    {
        savedlinks.getListOfKeywords(_getCSSBookmarksPath(), _fillBookmarkListCallback);
    }
    
    /**
     * Callback function called when the bookmarks are loaded.
     */
    function _fillBookmarkListCallback(keywordArray)
    {
        bookmarkArrayInstance = keywordArray;
        for (var i=0; i < keywordArray.length; i++)
        {
            _addKeywordToList(keywordArray[i]);
        }
        
        _bindBookmarks();
    }
    
    /**
     * Binds the bookmar elements to their functions.
     */
    function _bindBookmarks() {
        $(".bookmark-remove").on("click", function() {
            var bookmarkToRemove = $(this).attr("data-remove");
            _removeBookmark(bookmarkToRemove);
        });
        
        $(".bookmark-redirect").on("click", function() {
            var bookmarkToRedirect = $(this).attr("data-redir");
            _getCodeHintForElement(bookmarkToRedirect);
        });
    }
    
    /**
     * Remove a bookmark.
     * @param {string} bookmarkToRemove
     */
    function _removeBookmark(bookmarkToRemove) {
        savedlinks.removeBookmark(_getCSSBookmarksPath(), bookmarkToRemove);
        _removeKeywordFromList(bookmarkToRemove);
    }
    
    /**
     * Adds a particular keyword to the list.
     * @param {string} keywordToAdd
     */
    function _addKeywordToList (keywordToAdd) {
        var listId = "bookmarkelem-" + keywordToAdd;
        savedLinksList.append('<li id="'+listId+'"><a href="#" class="bookmark-remove" data-remove="'+ keywordToAdd + '" title="Remove"><i class="fa fa-bookmark"></i></a> <a href="" class="bookmark-redirect" data-redir="'+keywordToAdd+'" title="Show Hint">'+ keywordToAdd +'</a></li>');
    }
    
    /**
     * Remove a bookmark from the HTML list.
     * @param {string} bookmarkToRemove
     */
    function _removeKeywordFromList(bookmarkToRemove) {
        var listId = "#bookmarkelem-" + bookmarkToRemove;
        $(listId).remove();
    }
    
    /**
     * Gets the path where the bookmarks are stored.
     */
    function _getCSSBookmarksPath() {
        var extPath = ExtensionLoader.getUserExtensionPath();
        var userDataPath = extPath+"/"+utils.getExtensionDirectory()+"/UserData/CSSKeywords.txt";   
        return userDataPath;
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCSSCodeHinter()
    {
        CommandManager.register("CSS Code Hinter", CSS_CODE_HINT_EXECUTE, _togglePanel);
        //Adds menu item to the Streamliner Top-Level menu
        global.menu.addMenuItem(CSS_CODE_HINT_EXECUTE, "Ctrl-Shift-E");
        
        
        panel = WorkspaceManager.createBottomPanel(HINT_PANEL, $(panelCSS),200);
        
        var closeBtn = panel.$panel.find(".snippets-close");
        closeBtn.on("click", function(){
            _hidePanel();
        });
        
        var toggleSavedLinksBtn = panel.$panel.find("#toggle-saved");
        toggleSavedLinksBtn.on("click", function() {
            panel.$panel.find("#saved-links-container").toggle();
            if ($(this).find('i').hasClass("fa-toggle-off")) {
                $(this).html('<i class="fa fa-toggle-on"></i>');
            } else {
                $(this).html('<i class="fa fa-toggle-off"></i>');
            }
        });
        
        var clearBtn = panel.$panel.find("#hints-clearbtn");
        clearBtn.on("click", function(){
            panel.$panel.find("#hint-mdn").html('<div class="alert alert-info">Code documentation will appear here. Start typing to get a list of code hints and select the one you want to see.</div>');
            panel.$panel.find("#hint-schools").html('<div class="alert alert-info">Code documentation will appear here. Start typing to get a list of code hints and select the one you want to see.</div>');
            documentationLoaded = false;
        });
        
        panel.$panel.find(".collapsable").on("click", function(){
            $(this).siblings(".hint-block").slideToggle('slow', function(){
                if ($(this).is(":visible")){
                    $(this).parent().find("i").removeClass("fa-caret-down");
                    $(this).parent().find("i").addClass("fa-caret-up");
                }else{
                    $(this).parent().find("i").removeClass("fa-caret-up");
                    $(this).parent().find("i").addClass("fa-caret-down");
                }
            });
        });
        
        savedLinksList = panel.$panel.find("#saved-links-list");
        _loadBookmarkedList();
        
        //Saving bookmarks
        bookmarkElement = panel.$panel.find(".bookmark");
        panel.$panel.find("#bookmark-source").on("click", function() {
            if (savedLinksList.children("#bookmarkelem-"+loadedKeyword).length > 0) {
                return;
            }
            
            _saveCSSKeyword(loadedKeyword);
            _addKeywordToList(loadedKeyword);
            _bindBookmarks();
            _markBookmark();
        });
        
        //Help
        var helpElement = panel.$panel.find("#launch-help");
        helpElement.on("click", function() {
            Launcher.launch('http://cesareparra.com/streamliner/documentation/help.html');
        });
        
        documentableElementsArray = documentable.getDocumentableCSSFeatures();
    
        var ecdhints = new ECDHint();
        CodeHintManager.registerHintProvider(ecdhints, ["css"], 10);
        
    }
    
    /**
     * Public functions of this module.
     */
    module.exports = {
        registerCSSCodeHinter: _registerCSSCodeHinter
    };
});
