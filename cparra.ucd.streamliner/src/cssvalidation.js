define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    CodeInspection = brackets.getModule("language/CodeInspection"),
    DocumentManager         = brackets.getModule("document/DocumentManager"),
    AppInit = brackets.getModule("utils/AppInit");

    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    require('src/utils/csslint/csslint');
    
    var CSSVALIDATE_EXECUTE = "streamliner.cssvalidation.execute";
    
    /**
     * Performs CSS lint for the current file.
     */
    function _validateCSS()
    {
        //Get last Editor that had focus
        var editor = EditorManager.getActiveEditor();
        //Get contents of the document
        var documentText = editor.document.getText();
    
        // Execute CSSLint
		var results = CSSLint.verify(documentText);

		if (results.messages.length) 
        {
			var result = { errors: [] };

			for(var i=0; i<results.messages.length; i++) {
				var messageOb = results.messages[i];

				if(!messageOb.line) continue;
				//default
				var type = CodeInspection.Type.WARNING;

				if(messageOb.type === "error") {
					type = CodeInspection.Type.ERROR;
				} else if(messageOb.type === "warning") {
					type = CodeInspection.Type.WARNING;
				}

				var message = messageOb.rule.name + " - " + messageOb.message;
				message += " (" + messageOb.rule.id + ")";

				result.errors.push({
					pos: {line:messageOb.line-1, ch:messageOb.col},
					message:message,

					type:type
				});
			}
            
			return result;
		} 
        else 
        {            
			//no errors
			return null;
		}   
    }
    
    /**
     * Registers the call to validateCSS for this feature in order to take advantage
     * of the CodeInspector built in brackets functionality.
     */
    function _inspectCode()
    {
        CodeInspection.register("css", { name: "CSS Validation", scanFile: _validateCSS });
    }
    
    /**
     * Register this feature into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Valdiate CSS", CSSVALIDATE_EXECUTE, _inspectCode);
        global.menu.addMenuItem(CSSVALIDATE_EXECUTE);
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };

});