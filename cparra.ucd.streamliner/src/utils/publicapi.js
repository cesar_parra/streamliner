/**
 * Used to interact with the Streamliner API
 */
define(function(require, exports, module) {

    var utils = require("src/utils/utils");
    
    var apiURL = utils.getAPIPath();
    
    /**
     * Add a collection to the database.
     * @param {string[]} collection - The collection to save.
     */
    function _addCollection(collection)
    {
        $.post(apiURL+"setfontcollection.php", { "collection": JSON.stringify(collection) }, 
               function(data) {
                    //Success
                });
    }
    
    /**
     * Gets popular font collections from the database.
     */
    function _getPopularFonts()
    {
        $.get(apiURL + "popularFonts.php", function( data ) {
            var parsedData = JSON.parse(data);
            utils.log(parsedData.items[0].family);
        });
    }
    
    /**
     * Gets popular font collections inserted into the database.
     */
    function _getLatestCollections(callback)
    {
        $.get(apiURL + "latestCollections.php", function( data ) {
            var parsedData = JSON.parse(data);
        }).done(function(data){
            var parsedData = JSON.parse(data);
            callback(parsedData);
        });
    }
    
    /**
     * Add a palette into the database.
     * @param {string} paletteName - The name of the palette to save.
     * @param {string[]} paletteArray - The palette information to save.
     */
    function _addPalette(paletteName, paletteArray)
    {
        $.post(apiURL+"setpalette.php", { "name": paletteName, "palette": paletteArray.join(",") }, 
               function(data) {
                    //Success
                    utils.log("Add palette sent: " + paletteArray.join(","));
                    utils.log("Add palette API says: "+data);
                });
    }
    
    /**
     * Gets the latest palettes inserted into the database.
     */
    function _getLastestPalettes(callback)
    {
        $.get(apiURL + "latestPalettes.php", function( data ) {
            //Loading palettes
        }).done(function(data){
            var parsedData = JSON.parse(data);
            callback(parsedData);
        });
    }

    /**
     * Public functions of the module.
     */
    module.exports = {
        addCollection: _addCollection,
        getPopularFonts: _getPopularFonts,
        getLatestCollections: _getLatestCollections,
        addPalette: _addPalette,
        getLastestPalettes : _getLastestPalettes
    };

});
 