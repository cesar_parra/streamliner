/**
 * Used to call documentation sources.
 */
define(function (require, exports, module) {
    "use strict";
    
    var utils = require("src/utils/utils");
    
    var MDN_URL = "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/";
    var CSS_MDN_URL = "https://developer.mozilla.org/en-US/docs/Web/CSS/";
    var W3C_URL = "http://www.w3.org/TR/html5/single-page.html";
    var W3SCHOOLS_URL = " http://www.w3schools.com/tags/";
 
    /**
     * Creates the url to call the Mozilla Developer Network for HTML elements.
     */
    function _createMDNURL(elementName)
    {
        var baseURL = MDN_URL;
        baseURL = baseURL+elementName;
        return baseURL;
    }
    
    /**
     * Creates the url to call the Mozilla Developer Network for CSS elements.
     */
    function _createMDNCSSURL(featureName)
    {
        var baseURL = CSS_MDN_URL;
        baseURL = baseURL + featureName;
        return baseURL;
    }
    
    /**
     * Creates the url to call the W3C for HTML elements.
     */
    function _createW3CURL(elementName)
    {
        var baseURL = W3C_URL;
        return baseURL;
    }
    
    /**
     * Creates the url to call the w3Schools for HTML elements.
     */
    function _createW3SchoolsURL(elementName)
    {
        return "http://www.w3schools.com/tags/tag_"+elementName+".asp";
    }
    
    /**
     * Creates the url to call the Mozilla Developer Network for CSS elements.
     */
    function _createW3SchoolsCSSURL1(featureName)
    {
        return  "http://www.w3schools.com/cssref/pr_"+featureName+".asp";
    }
    
    /**
     * Creates the url to call the Mozilla Developer Network for CSS elements.
     */
    function _createW3SchoolsCSSURL2(featureName)
    {
        return  "http://www.w3schools.com/cssref/css3_pr_"+featureName+".asp";
    }
    
    /**
     * Gets the contents of a webpage.
     * @param {string} url - the url of the webpage
     * @param {doneCallback} function - the function to call when the contents are loaded.
     */
    function _getWebPageContent(url, doneCallback)
    {   
        $.get(url, function( data ) {
            //Data: HTML Content of the Page
        }).done(function(data){
            
            doneCallback(data);
            
        }).fail(function(){
            utils.log("Failed to load the web page content");
        });
    }

    /**
     * Public functions of this module.
     */
    module.exports = {
        getWebPageContent: _getWebPageContent,
        createMDNURL : _createMDNURL,
        createW3CURL : _createW3CURL,
        createW3SchoolsURL : _createW3SchoolsURL,
        createMDNCSSURL : _createMDNCSSURL,
        createW3SchoolsCSSURL1 : _createW3SchoolsCSSURL1,
        createW3SchoolsCSSURL2 : _createW3SchoolsCSSURL2
    };
    
});