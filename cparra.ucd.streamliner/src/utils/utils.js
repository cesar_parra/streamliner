/**
 * General util functions.
 */
define(function(require, exports, module) {
    
    var PreferencesManager = brackets.getModule("preferences/PreferencesManager")
    var prefs = PreferencesManager.getExtensionPrefs("streamliner");
    
    /**
     * Log into the console with the "Streamliner" name.
     */
    function _log(s) {
            console.log("[Streamliner] "+s);
    }
    
    /**
     * Gets the directoy of this extension.
     */
    function _getExtensionDirectory(){
        return "cparra.ucd.streamliner";
    }
    
    /**
     * Gets the API url.
     */
    function _getAPIPath()
    {
        return "http://cesareparra.com/streamliner/api/";
    }
    
    /**
     * Gets the preference for sharing color palettes.
     */
    function _isColorShared()
    {
        return prefs.get("shareColors");
    }
    
    /**
     * Gets the preference for sharing font collections.
     */
    function _isFontShared()
    {
        return prefs.get("shareFonts");
    }
    
    /**
     * Sets the preference for sharing color palettes.
     */
    function _setColorShared(share)
    {
        prefs.set("shareColors", share);
    }
    
    /**
     * Sets the preference for sharing font collections.
     */
    function _setFontShared(share)
    {
        prefs.set("shareFonts", share);
    }
     
    /**
     * Public functions of this module.
     */
    module.exports = {
        log: _log,
        getExtensionDirectory: _getExtensionDirectory,
        getAPIPath: _getAPIPath,
        isColorShared: _isColorShared,
        isFontShared: _isFontShared,
        setColorShared: _setColorShared,
        setFontShared: _setFontShared
    };
});