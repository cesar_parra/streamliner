define(function(require, exports, module) {

    var FileSystem = brackets.getModule("filesystem/FileSystem"),
        FileUtils = brackets.getModule("file/FileUtils");

    /**
     * Save a new collection created by the user.
     * @param {string} path - the path to save the collection.
     * @param {string[]} collection - the collection to save
     */
    function _writeCollection(path, collection)
    {
        var textToWrite = "";
        
        Object.keys(collection).forEach(function(key, url){
            textToWrite += collection[key] + ",";
        });
        textToWrite += _getCollectionsDate();
        
        var existingFile = FileSystem.getFileForPath(path);
        var existingText;
        existingFile.read({}, function(smth, text){
            existingText = text;
            console.log("[Existing Text]" + existingText);
            
            if (existingText) textToWrite = existingText + "\r\n" + textToWrite;
            
            FileUtils.writeText(existingFile, textToWrite, true);
        });
    }
    
    /**
     * Reads the user's saved collection
     * @param {string} path - the path to look for the collection.
     * @param {function} callback - the function to call when the file is read.
     */
    function _readCollections(path, callback)
    {
        //Return array with collections
        var collectionsFile = FileSystem.getFileForPath(path);
        collectionsFile.read({}, function (smth, text){
            var fileText = text;
            
            var collectionsArray = fileText.split("\r\n");
            
            callback(collectionsArray);
            
        });
    }
    
    /**
     * Gets todays date in a readable format.
     */
    function _getCollectionsDate()
    {
        var date = new Date();
        return date.getMonth()+1 + "-" + date.getDate() + "-" + date.getFullYear();
    }
    
    /**
     * Inserts a new palette
     * @param {string} path - The path to write the pallete to.
     * @param {string} paletteName - The name of the palette to create.
     * @param {string[]} paletteArray - The string array containing the palette information.
     */
    function _writePalette(path, paletteName, paletteArray)
    {
        var textToWrite = "";
        
        var paletteString = paletteArray.join(',');
        
        textToWrite = paletteName + ';' + paletteString + ';' +_getCollectionsDate();
        
        var existingFile = FileSystem.getFileForPath(path);
        var existingText;
        existingFile.read({}, function(smth, text){
            existingText = text;
            
            if (existingText) textToWrite = existingText + "\r\n" + textToWrite;
            
            FileUtils.writeText(existingFile, textToWrite, true);
        });
    }
    
    /**
     * Reads a file for palettes.
     * @param {string} path - The path to read the pallete from.
     * @param {function} callback - The function to call when the file is read.
     */
    function _readPersonalPalettes(path, callback)
    {
        var palettesFile = FileSystem.getFileForPath(path);
        palettesFile.read({}, function(smth, text){
            var fileText = text;
            var paletteLinesArray = fileText.split('\r\n');
            callback(paletteLinesArray);
        });
    }
    
    /**
     * Public functions of this module.
     */
    module.exports = {
        writeCollection: _writeCollection,
        readCollections: _readCollections,
        writePalette: _writePalette,
        readPersonalPalettes: _readPersonalPalettes
    };

});
