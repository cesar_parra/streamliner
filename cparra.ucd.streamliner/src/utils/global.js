define(function(require, exports, module) {

    var Menus = brackets.getModule("command/Menus")
    
    //Add the top-level menu
    var _menu = Menus.addMenu("Streamliner", "cparra.streamliner.streamliner", Menus.AFTER, Menus.AppMenuBar.NAVIGATE_MENU);
    
    /**
     * Public functions of this module.
     */
    module.exports = {
        menu: _menu
    };

});