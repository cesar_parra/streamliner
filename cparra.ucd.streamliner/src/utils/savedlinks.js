/**
 * Controls saving bookmarks to local files.
 */
define(function(require, exports, module) {

    var FileSystem = brackets.getModule("filesystem/FileSystem"),
        FileUtils = brackets.getModule("file/FileUtils");

    /**
     * Save a keyword into a file
     * param {string} path - The path to save the keyword to,
     * param {string} keyword - The keyword to save.
     */
    function _saveSource(path, keyword)
    {   
        var textToWrite = keyword;
        
        var existingFile = FileSystem.getFileForPath(path);
        var existingText;
        existingFile.read({}, function(smth, text){
            existingText = text;
            
            if (existingText) textToWrite = existingText + "\r\n" + textToWrite;
            
            FileUtils.writeText(existingFile, textToWrite, true);
        }); 
    }
    
    /**
     * Clears the content of a file and overrides it with the keywordArray.
     * param {string} path - The path to save the keyword to.
     * param {string[]} keywordArray - The keyword array to save.
     */
    function _overrideSource(path, keywordArray)
    {
        var existingFile = FileSystem.getFileForPath(path);
        existingFile.read({}, function(smth, text){
            var textToWrite = "";
            for (var i=0; i<keywordArray; i++)
            {
                textToWrite+= keywordArray[i] + "\r\n";
            }
            
            FileUtils.writeText(existingFile, textToWrite, true);
        }); 
    }
    
    /**
     * Gets the list of keywords on a file.
     * param {string} path - The file to use.
     * param {function} callback - The function to call when the file is read.
     */
    function _getListOfKeywords(path, callback)
    {
        var sourceFile = FileSystem.getFileForPath(path);
        sourceFile.read({}, function(smth, text) {
            var fileText = text;
            var keywordsArray = fileText.split("\r\n");
            
            callback(keywordsArray);
        });
    }
    
    /**
     * Removes a bookmark from a file
     * param {string} path - The file to use.
     * param {string} bookmarkToRemove - The keyword to remove.
     */
    function _removeBookmark(path, bookmarkToRemove)
    {
        var existingFile = FileSystem.getFileForPath(path);
        var existingText;
        existingFile.read({}, function(smth, text){
            existingText = text;
            var existingTextArray = existingText.split("\r\n");
            var keywordPosition = existingTextArray.indexOf(bookmarkToRemove);
            if (keywordPosition > -1) {
                existingTextArray.splice(keywordPosition, 1);
            }
            
            if (existingTextArray.length > 0) {
                var textToWrite = existingTextArray[0];
                for (var i=1; i<existingTextArray.length; i++) {
                    textToWrite = existingTextArray[i] + "\r\n" + textToWrite;
                }

                FileUtils.writeText(existingFile, textToWrite, true);
            } else {
                FileUtils.writeText(existingFile, "", true);
            }
        }); 
    }
    
    /**
     * Public functions of the module.
     */
    module.exports = {
        saveSource: _saveSource,
        getListOfKeywords: _getListOfKeywords,
        overrideSource: _overrideSource,
        removeBookmark: _removeBookmark
    };
});
