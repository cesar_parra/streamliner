/* Creating a palette list item
<li class="card">
    <h4>Palette Name</h4>
    <div class="pal-added-block"></div>
    <div class="pal-added-block"></div>
    <a href="" class="stream-add"><i class="fa fa-plus-circle"></i></a>
</li>
*/

define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
    Menus = brackets.getModule("command/Menus"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    DefaultDialogs = brackets.getModule("widgets/DefaultDialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    WorkspaceManager = brackets.getModule("view/WorkspaceManager"),
    ExtensionUtils = brackets.getModule("utils/ExtensionUtils"),
    KeyBindingManager = brackets.getModule("command/KeyBindingManager"),
    CodeInspection = brackets.getModule("language/CodeInspection"),
    ExtensionLoader = brackets.getModule("utils/ExtensionLoader"),
    AppInit = brackets.getModule("utils/AppInit");
    
    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    var publicapi = require("src/utils/publicapi");
    var fontcollection = require("src/utils/fontcollection");
    require("src/utils/colorpicker/js/colpick");
    
    
    var COLOR_EXECUTE = "streamliner.colorpalette.execute";
    var palette_dialog = require("text!html/palette.html");
    var paletteDialog;
    
    //Dictionary of all the palettes added
    //Key: paletteName, value: array of strings
    var paletteDictionary = {};
    
    var selectedColors = [];
    var paletteName;
    
    //Enumeration for Page Ids
    var Page = {
        CREATE_PAGE       : "#pal-page-create",
        BROWSE_PAGE : "#pal-page-browse",
        PERSONAL_PAGE : "#pal-page-personal"
    };
    
    /**
     * Opens a page in the Palette window
     * @param {string} pageId - The HTML id of the page to open
     */
    function _showPage(pageId)
    {
        //Hide all the pages
        $.each(Page , function(key, value) { 
          $(value).fadeOut(600, function(){
            //Show only the desired page
            $(pageId).show();
          });
        });
    }
    
    /**
     * Save a palette.
     * @param {string} paletteName
     * @param {string[]} paletteArray
     */
    function _savePersonalPalette(paletteName, paletteArray)
    {
        var extPath = ExtensionLoader.getUserExtensionPath();
        userDataPath = extPath+"/"+utils.getExtensionDirectory()+"/UserData/Palettes.txt";
        
        fontcollection.writePalette(userDataPath, paletteName, paletteArray);
    }
    
    /**
     * Insert colors into the editor.
     */
    function _insertSelectedColors()
    {        
        if (selectedColors.length > 0 && paletteName == "")
        {
            alert("Give a name to your palette!");
        }
        else
        {
            if (selectedColors.length > 0)
            {
                paletteDictionary[paletteName] = selectedColors;
                //Only save a personal palette if the palette was created by the user
                _savePersonalPalette(paletteName, selectedColors);
            }
            
            var colorsToEditor = [];
            Object.keys(paletteDictionary).forEach(function(palName, index){

                var currentArray = paletteDictionary[palName];
                
                if (utils.isColorShared()) {
                    publicapi.addPalette(palName, currentArray);
                }
                
                for (var i = 0; i<currentArray.length; i++)
                {
                    colorsToEditor.push(currentArray[i]);
                }            
            });
            
            var editor = EditorManager.getActiveEditor();
            if (editor) {
                // Get the cursor position
                var cursor = editor.getCursorPos();

                colorsToEditor.forEach(function (value) {
                    editor.document.batchOperation(function() {
                        editor.document.replaceRange("#"+value+"\n", cursor);
                    });
                });
            }
            
            //Close the dialog when finished inserting the colors
            paletteDialog.close(); 
        }    
    }
    
    /**
     * Applies the ".selected" class to show the current page.
     * @param {string} selectedId - The id of the page. 
     */
    function _applySelectedClass(selectedId) {
        $(".selected").removeClass("selected");
        $(selectedId).addClass("selected");
    }
    
    /**
     * Toggles the Palette Dialog window.
     */
    function _toggleColorPaletteDialogWindow()
    {
        //Clean colors
        paletteDictionary = {};
        selectedColors.length = 0;
        
        //Open the color palette window
        paletteDialog = Dialogs.showModalDialogUsingTemplate(palette_dialog, true),
        $closeBtn = paletteDialog.getElement().find("#close-btn");
        $actionBtn = paletteDialog.getElement().find("#action-btn");
        
        // Bind the close button
        $closeBtn.on("click", paletteDialog.close.bind(paletteDialog));
        
        //Load what needs to be loaded
        _loadPicker();
        _loadBrowsePage();
        _loadPersonalPage();
        
        // Upon closing the dialog, run function to gather and apply choices
        $actionBtn.on("click", function(){
            paletteName = paletteDialog.getElement().find("#pal-usergiven-name").val();
            _insertSelectedColors();
        });
        
        $homeLink = paletteDialog.getElement().find("#navigate-home");
        $homeLink.on("click", function() {
            _showPage(Page.CREATE_PAGE);
            _applySelectedClass("#navigate-home");
        });
        
        $browseLink = paletteDialog.getElement().find("#navigate-browse");
        $browseLink.on("click", function() {
            _showPage(Page.BROWSE_PAGE);
            _applySelectedClass("#navigate-browse");
        });
        
        $personalLink = paletteDialog.getElement().find("#navigate-personal");
        $personalLink.on("click", function() {
            _showPage(Page.PERSONAL_PAGE);
            _applySelectedClass("#navigate-personal");
        });
    }
    
    /**
     * Open the personal page.
     */
    function _loadPersonalPage()
    {
        var extPath = ExtensionLoader.getUserExtensionPath();
        userDataPath = extPath+"/"+utils.getExtensionDirectory()+"/UserData/Palettes.txt";
        
        fontcollection.readPersonalPalettes(userDataPath, _getPersonalPalettesCallback);
    }
    
    /**
     * Callback for when the personal palettes are loaded.
     */
    function _getPersonalPalettesCallback(paletteLinesArray)
    {       
        $palPersonal = paletteDialog.getElement().find("#pal-list-personal");
        for (var i=0; i<paletteLinesArray.length; i++)
        {
            utils.log(paletteLinesArray);
            var lineSplit = paletteLinesArray[i].split(';');
            var paletteName = lineSplit[0];
            var paletteColors = lineSplit[1].split(',');
            var paletteDate = lineSplit[2];
        
            var listElement = [];
            
             listElement.push('<li class="card">');
                listElement.push('<h5>Date Created: ' + paletteDate + '</h5>');
                listElement.push('<h4>'+paletteName+'</h4>');
            
            for (var j=0; j<paletteColors.length; j++)
            {
                listElement.push('<div class="pal-added-block" style="background-color: #'+paletteColors[j]+'"></div>');
            }
                listElement.push('<a href="" data-name="'+paletteName+'" data-colors="'+paletteColors+'" class="pal-add-my stream-add"><i class="fa fa-plus-circle"></i></a>');
            listElement.push('</li>');
            var finalListElement = listElement.join('');
            $palPersonal.append(finalListElement);
        
        }

        $('.pal-add-my').on("click", function(){
            //Adding palette
            if ($(this).find('i').hasClass('fa-plus-circle'))
            {
                var palDicKey = $(this).attr("data-name");
                var palDicColors = $(this).attr("data-colors");
                paletteDictionary[palDicKey] = palDicColors.split(',');
                utils.log(paletteDictionary[palDicKey] + " " + paletteDictionary[palDicKey].length);
            }
            //Removing palette
            else
            {
                var palDicKey = $(this).attr("data-name");
                delete paletteDictionary[palDicKey];
            }
            $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
        });
    }
    
    /**
     * Adds a preview colored block for the color added.
     */
    function _handleColorSelect(hsb,hex,rgb,el)
    {
        //Add to the selected colors array
        selectedColors.push(hex);
        
        $palSelectedBlock = paletteDialog.getElement().find("#pal-user-selections");
        
        $paletteToAdd = '<div class="pal-added-block" style="background-color:#'+hex+'"></div>';
        
        $palSelectedBlock.append($paletteToAdd);
    }
    
    /**
     * Load the color picker.
     */
    function _loadPicker()
    {
        $palContent = paletteDialog.getElement().find("#pal-content");
        
        var extPath = ExtensionLoader.getUserExtensionPath();
        var colPickerPath = extPath+"/"+utils.getExtensionDirectory()+"/src/utils/colorpicker/css/colpick.css";    

        $palContent.prepend("<link href='"+colPickerPath+"' rel='stylesheet' type='text/css'>");
        
        $('#picker').colpick({
            flat:true,
            colorScheme: 'dark',
            layout:'hex',
            submit:1,
            onSubmit:_handleColorSelect
        });
    }
    
    /**
     * Loads the browse page.
     */
    function _loadBrowsePage()
    {
        publicapi.getLastestPalettes(_loadBrowsePageCallback);
    }
    
    /**
     * Callback from when the browse page is loaded.
     */
    function _loadBrowsePageCallback(data)
    {
        $palBrowse = paletteDialog.getElement().find("#pal-list-browse");
        for (var i=0; i<data.items.length; i++)
        {
             var paletteName = data.items[i].name;
             var paletteDate = data.items[i].DateCreated;
             var paletteUses = data.items[i].uses;
             var paletteColors = data.items[i].palette.split(',');
            
            var listElement = [];
            
            listElement.push('<li class="card">');
                listElement.push('<h5>Date Created: ' + paletteDate + '</h5>');
                listElement.push('<h4>'+paletteName+'</h4>');
                listElement.push('<h5>Times used: '+paletteUses+'</h5>');
        
            for (var j=0; j<paletteColors.length; j++)
            {
                listElement.push('<div class="pal-added-block" style="background-color: #'+paletteColors[j]+'"></div>');
            }
                listElement.push('<a href="" data-name="'+paletteName+'" data-colors="'+paletteColors+'" class="pal-add stream-add"><i class="fa fa-plus-circle"></i></a>');
            listElement.push('</li>');
            var finalListElement = listElement.join('');
            $palBrowse.append(finalListElement);
        }
        
        $('.pal-add').on("click", function(){
            //Adding palette
            if ($(this).find('i').hasClass('fa-plus-circle'))
            {
                var palDicKey = $(this).attr("data-name");
                var palDicColors = $(this).attr("data-colors");
                paletteDictionary[palDicKey] = palDicColors.split(',');
                utils.log(paletteDictionary[palDicKey]);
            }
            //Removing palette
            else
            {
                var palDicKey = $(this).attr("data-name");
                delete paletteDictionary[palDicKey];
            }
            $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
        });
    }
    
    /**
     * Register this functioanlity into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Color Palette Tool...", COLOR_EXECUTE, _toggleColorPaletteDialogWindow);
        global.menu.addMenuItem(COLOR_EXECUTE, "Ctrl-Shift-P");
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };

});