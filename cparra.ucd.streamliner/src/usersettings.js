define(function(require, exports, module) {
    var CommandManager = brackets.getModule("command/CommandManager"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager");

    //Local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");

    //User Settings
    var SETTINGS_EXECUTE = "streamliner.settings.execute";
    var settings_dialog = require("text!html/usersettings.html");
    var settingsDialog;
    
    //Local variables
    var $colorCheckbox;
    var $fontCheckbox;
    
    /**
     * Toggles the settings dialog window.
     */
    function _toggleSettings() {
        //Show the Settings Dialog
        settingsDialog = Dialogs.showModalDialogUsingTemplate(settings_dialog, true),
            $closeBtn = settingsDialog.getElement().find("#close-btn");
            $actionBtn = settingsDialog.getElement().find("#action-btn");
            $colorCheckbox = settingsDialog.getElement().find("#share-colors");
            $fontCheckbox = settingsDialog.getElement().find("#share-fonts");
        
        if (utils.isColorShared()) {
            $colorCheckbox.prop('checked', true);
        } else {
            $colorCheckbox.prop('checked', false);
        }
        
        if (utils.isFontShared()) {
            $fontCheckbox.prop('checked', true);
        } else {
            $fontCheckbox.prop('checked', false);
        }
        
        // Bind the close button
        $closeBtn.on("click", settingsDialog.close.bind(settingsDialog));

        // Upon closing the dialog, run function to gather and apply choices 
        $actionBtn.on("click", savePreferences);    
    }
    
    /**
     * Save the preferences
     */
    function savePreferences() {
        //Save settings here
        utils.setColorShared($colorCheckbox.prop('checked'));
        utils.setFontShared($fontCheckbox.prop('checked'));
                            
        settingsDialog.close();
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Preferences...", SETTINGS_EXECUTE, _toggleSettings);
        //Adds menu item to the Streamliner Top-Level menu
        global.menu.addMenuItem(SETTINGS_EXECUTE);
    }
    
    /**
     * Public functions of this module.
     */
    module.exports = {
        id: SETTINGS_EXECUTE,
        registerCommand: _registerCommand,
        toggleSettings: _toggleSettings
    };
});