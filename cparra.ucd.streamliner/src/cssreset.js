define(function(require, exports, module) {
    
    var CommandManager = brackets.getModule("command/CommandManager"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager");
    
    //Local module
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    
    //CSS Reset
    var CSSRESET_EXECUTE = "streamliner.cssreset.execute";
    var cssreset_dialog = require("text!html/cssReset.html");
    var cssDialog;
    
    cssResetText= "/* http://meyerweb.com/eric/tools/css/reset/ 2. v2.0 | 20110126\n  License: none (public domain)\n*/\n\nhtml, body, div, span, applet, object, iframe,\nh1, h2, h3, h4, h5, h6, p, blockquote, pre,\na, abbr, acronym, address, big, cite, code,\ndel, dfn, em, img, ins, kbd, q, s, samp,\nsmall, strike, strong, sub, sup, tt, var,\nb, u, i, center,\ndl, dt, dd, ol, ul, li,\nfieldset, form, label, legend,\ntable, caption, tbody, tfoot, thead, tr, th, td,\narticle, aside, canvas, details, embed,\nfigure, figcaption, footer, header, hgroup,\nmenu, nav, output, ruby, section, summary,\ntime, mark, audio, video {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  font-size: 100%;\n  font: inherit;\n  vertical-align: baseline;\n}\n/* HTML5 display-role reset for older browsers */\narticle, aside, details, figcaption, figure,\nfooter, header, hgroup, menu, nav, section {\n  display: block;\n}\nbody {\n  line-height: 1;\n}\nol, ul {\n  list-style: none;\n}\nblockquote, q {\n  quotes: none;\n}\nblockquote:before, blockquote:after,\nq:before, q:after {\n  content: '';\n  content: none;\n}\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}";
    
    /**
     * Inserts the CSS Reset into the current document.
     */
    function insertCSSResetCode()
    {
        var editor = EditorManager.getActiveEditor();
        
        if (editor) {
            // Get the cursor position
            var cursor = editor.getCursorPos();

            editor.document.batchOperation(function() {
            // Insert the selected elements at the current cursor position
                editor.document.replaceRange(cssResetText, cursor);
            });                       
        }
        
        cssDialog.close();   
    }
    
    /**
     * Toggles the CSS Reset dialog window.
     */
    function _toggleCSSResetDialogWindow() {
        //Open the CSS Reset Dialog Window
         
        //Show the CSS Reset Dialog
        cssDialog = Dialogs.showModalDialogUsingTemplate(cssreset_dialog, true),
        $closeBtn = cssDialog.getElement().find("#close-btn");
        $actionBtn = cssDialog.getElement().find("#action-btn");
        
        // Bind the close button
        $closeBtn.on("click", cssDialog.close.bind(cssDialog));

        // Upon closing the dialog, run function to gather and apply choices
        $actionBtn.on("click", insertCSSResetCode);
    }
    
    /**
     * Register this functioality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Insert CSS Reset...", CSSRESET_EXECUTE, _toggleCSSResetDialogWindow);
        global.menu.addMenuItem(CSSRESET_EXECUTE);
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        id: CSSRESET_EXECUTE,
        registerCommand: _registerCommand,
        toggleCSSResetDialogWindow: _toggleCSSResetDialogWindow
    };
});