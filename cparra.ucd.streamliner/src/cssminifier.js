define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    MainViewManager = brackets.getModule("view/MainViewManager"),
    FileSystem = brackets.getModule("filesystem/FileSystem"),
    DocumentManager = brackets.getModule("document/DocumentManager"),
    ProjectManager = brackets.getModule("project/ProjectManager"),
    AppInit = brackets.getModule("utils/AppInit");
    
    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    var cssmin = require("src/utils/minify/cssmin").CSSMin;
    
    var CSSMINIFY_EXECUTE = "streamliner.cssminifier.execute";
    var minifier_dialog = require("text!html/minifier.html");
    var minifierDialog;
    
    /**
     * Minifies a document
     * @param editor - The brackets editor.
     */
    function _minify(editor)
    {
        var file = editor.document.file;
        file.read({}, function(arg,contents,fs){
            $miniOldSize = minifierDialog.getElement().find(".mini-oldsize").html(fs.size); 
        });
        var originalFullPath = file.fullPath;
        
        //Minify the text of the document
        var minified = cssmin.go(editor.document.getText());
        //Save the a minified copy of the file
        var path = file.fullPath.replace(".css", ".min.css");
        
        
        var file = FileSystem.getFileForPath(path).write(minified, {}, function(arg, fs){
            ProjectManager.refreshFileTree;
            $miniNewSize = minifierDialog.getElement().find(".mini-newsize").html(fs.size); 
        });
        
        $miniPath = minifierDialog.getElement().find(".mini-path"); 
        $miniPath.html('<a href="'+path+'">'+path+'</a>');
        $statusSuccess = minifierDialog.getElement().find(".mini-success");
        $statusSuccess.show();
        
        utils.log("Minified complete");
    }
    
    /**
     * Verifies if the minifier can be called for this document.
     */
    function _process()
    {   
        //Find out if the extension is css
        extension = (EditorManager.getActiveEditor()).document.file.name.split('.').pop();
        if (extension == "css")
        {
            //File can be minified
            var editor = EditorManager.getActiveEditor();
            _minify(editor);
        }
        else{
            //File cannot be minified
             $statusError = minifierDialog.getElement().find(".mini-error");
             $statusError.show();
        }
    }
    
    /**
     * Toggles the minifier dialog window.
     */
    function _toggleMinifierDialogWindow()
    {
        //Show the Minifier Dialog
        minifierDialog = Dialogs.showModalDialogUsingTemplate(minifier_dialog, true),
        $closeBtn = minifierDialog.getElement().find("#close-btn");
        $actionBtn = minifierDialog.getElement().find("#action-btn");
        
        // Bind the close button
        $closeBtn.on("click", minifierDialog.close.bind(minifierDialog));

        // Upon closing the dialog, minify
        $actionBtn.on("click", _process);
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Minify CSS...", CSSMINIFY_EXECUTE, _toggleMinifierDialogWindow);
        global.menu.addMenuItem(CSSMINIFY_EXECUTE, "Ctrl-Shift-M");
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };

});