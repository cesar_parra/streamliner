define(function(require, exports, module) {
    
    var CommandManager = brackets.getModule("command/CommandManager"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager");

    //Local modules
    var global = require("src/utils/global");

    //Skeleton
    var SKELETON_EXECUTE = "streamliner.skeleton.execute";
    var skeleton_dialog = require("text!html/skeleton.html");
    var skDialog;
    //var dialogOpen = false;
    
    var fourSpaceIndent = "\u0020\u0020\u0020\u0020";
    
    var skeletonComponents = [
            // If the full thing was selected
            '<!DOCTYPE html>\n<html lang="">\n<head>\n' + fourSpaceIndent +
                '<meta charset="utf-8">\n' + fourSpaceIndent + '<title></title>\n' + fourSpaceIndent + 
                '<link rel="stylesheet" href="">' +
                '\n</head>\n\n<body>\n' + fourSpaceIndent + '\n</body>\n</html>\n',

            // If doctype was selected
            '<!DOCTYPE html>',

            // External Stylesheet
            '<link rel="stylesheet" href="">',

            // External script
            '<script src=""></script>',

            // Body
            '<body></body>',
        
            //Lorem ipsum
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla blandit tempus erat, vel varius diam. Curabitur semper orci et eros tempus, vitae lobortis turpis egestas. Proin congue ante at arcu scelerisque, eget pellentesque ante venenatis. Nulla ultrices justo vel interdum hendrerit. Integer imperdiet tortor eget dignissim venenatis. Phasellus tincidunt fringilla posuere. Pellentesque commodo tellus quis rutrum tempus. Praesent vestibulum molestie nisl tincidunt sollicitudin. In euismod consectetur elit, ac tempor felis pellentesque scelerisque. Fusce tempus metus varius, euismod felis ut, facilisis justo. Integer gravida tristique justo id tempor. Maecenas varius rutrum mauris, vel imperdiet risus luctus lobortis. Vivamus nisl tortor, facilisis sit amet neque vestibulum, condimentum ultrices magna. Sed leo tortor, tincidunt viverra faucibus eu, blandit sed dolor.'
        ];
    //Store the selected elements
    var finalElements = [];
    
    /**
     * Insert selected elements into the editor.
     */
    function insertSelected() {
        //Insert the elements into the document

        // Get the last active editor because the dialog steals focus
        var editor = EditorManager.getActiveEditor();
        if (editor) {
            // Get the cursor position
            var cursor = editor.getCursorPos();

            finalElements.forEach(function (value) {
                //  Wrap the actions in a `batchOperation`, per guidelines
                editor.document.batchOperation(function() {
                    // Insert the selected elements at the current cursor position
                    editor.document.replaceRange(value, cursor);
                });
            });
        }
        
        //Close the dialog when finished inserting the code
        skDialog.close();
    }
    
    /**
     * Get the selected options from the UI.
     */
    function getSelectedOptions()
    {
        //Clear array
        finalElements.length = 0;
        
        //All possible options
        var optionIDs = ["#full-skeleton", "#doctype", "#x-style",
                         "#x-script", "#skel-body", "#skel-lorem"];
        
        // For each option that is checked, add the corrisponding element
        // to `finalElements` for addition in document
        optionIDs.forEach(function (value, index) {
            if ($(value + ":checked").val() === "on") {
                finalElements.push(skeletonComponents[index]);
            }
        });

        // Finally, run process to add the selected elements
        insertSelected();
    }
    
    /**
     * Replaces HTML tags for their respective codes in order to display them.
     * @param {string} str - The element to clean.
     */
    function safe_tags(str) {
        return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
    }
    
    /**
     * Updates the preview block.
     */
    function _updatePreview() {
        $previewPanel = skDialog.getElement().find("#skel-preview");
        $previewPanel.html("");
        var previewText = "";
        finalElements.forEach(function (value) {
            var escapedTags = safe_tags(value);
            previewText += escapedTags;
        });
        if (previewText == "") {
            $previewPanel.html('<div class="alert-info">There is nothing to preview yet. Start adding elements to see the code that will be inserted.</div>');
        }
        else {
            $previewPanel.html(previewText);
        }
        
    }
    
    /**
     * Toggles the skeleton.
     */
    function _toggleSkeleton() { 
        //Clear elements
        finalElements.length = 0;
        
        //Open the Skeleton Dialog
        
        //Show the Skeleton Dialog
        skDialog = Dialogs.showModalDialogUsingTemplate(skeleton_dialog, true),
            $closeBtn = skDialog.getElement().find("#close-btn");
            $actionBtn = skDialog.getElement().find("#action-btn");
            $addLink = skDialog.getElement().find(".skel-elem-add");
        //dialogOpen = true;
        
        // Bind the close button
        $closeBtn.on("click", skDialog.close.bind(skDialog));

        // Upon closing the dialog, run function to gather and apply choices
        //$actionBtn.on("click", getSelectedOptions);    
        $actionBtn.on("click", insertSelected);    
        
        $addLink.on("click", _toggleAdd);
    }
    
    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
    
    /**
     * Toggle add/remove of an element.
     */
    function _toggleAdd() {  
        var indexToAdd = $(this).attr("data-index");
        if ($(this).find('i').hasClass('fa-plus-circle'))
        {
           //Adding to the array
           finalElements.push(skeletonComponents[indexToAdd]); 
        }
        else {
            //Removing from the array
            finalElements.remove(skeletonComponents[indexToAdd]);
        }
        
        _updatePreview();
        
        $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Insert HTML Skeleton...", SKELETON_EXECUTE, _toggleSkeleton);
        //Adds menu item to the Streamliner Top-Level menu
        global.menu.addMenuItem(SKELETON_EXECUTE, "Ctrl-Shift-I");
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        id: SKELETON_EXECUTE,
        registerCommand: _registerCommand,
        toggleSkeleton: _toggleSkeleton
    };
});