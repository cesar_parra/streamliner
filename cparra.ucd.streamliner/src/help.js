define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
        Launcher       = brackets.getModule("LiveDevelopment/MultiBrowserImpl/launchers/Launcher");
    
    var OPEN_HELP = 'streamliner.openhelp';
    
    var global = require("src/utils/global");
    
    /**
     * Open the help url.
     */
    function _openHelpUrl() {
        Launcher.launch('http://cesareparra.com/streamliner/documentation/help.html');
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Help...", OPEN_HELP, _openHelpUrl);
        global.menu.addMenuItem(OPEN_HELP);
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };

});