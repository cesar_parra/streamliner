define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    CodeInspection = brackets.getModule("language/CodeInspection"),
    DocumentManager         = brackets.getModule("document/DocumentManager"),
    AppInit = brackets.getModule("utils/AppInit");

    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    require('src/utils/w3validation');
    
    
    var HTMLVALIDATE_EXECUTE = "streamliner.htmlvalidation.execute";
    
    /**
     * Calls the W3C Validator on this file.
     */
    function _validateHTML()
    {   
        //Get last Editor that had focus
        var editor = EditorManager.getActiveEditor();
        //Get contents of the document
        var documentText = editor.document.getText();
        
        
        var response = new $.Deferred();
        var result = {errors:[]};
        
        W3CValidator.validate(documentText, function (res) {
            
            var messages = res.messages;
            
            if (messages.length) {
                messages.forEach(function (item) {
                    utils.log('W3CValidation messsage ' + item.message + ' ' + item.type);
                    
                    var type = CodeInspection.Type.ERROR;
                    
                    if (item.type == "warning")
                        type = CodeInspection.Type.WARNING;
                    
                    result.errors.push({
                        pos: {line:item.lastLine-1, ch:item.lastColumn-1},
                        message:item.message,
                        type:type
                    });
                    
                });
            }

            response.resolve(result);
        });
        
        //var currentDoc = DocumentManager.getCurrentDocument();
        //currentDoc.notifySaved();
        
        return response.promise();
    }
    
    /**
     * Registers validateHTML into the CodeInspector in order to take advantage of
     * the built in Brackets CodeInspection.
     */
    function _inspectCode()
    {
        CodeInspection.register("html", { name: "HTML Validation", scanFileAsync: _validateHTML });
    }
    
    /**
     * Register this functionality into the app.
     */
    function _registerCommand()
    {
        CommandManager.register("Valdiate HTML", HTMLVALIDATE_EXECUTE, _inspectCode);
        global.menu.addMenuItem(HTMLVALIDATE_EXECUTE);
    }
    
    /**
     * Public functions for this module.
     */
    module.exports = {
        registerCommand: _registerCommand,
    };
});