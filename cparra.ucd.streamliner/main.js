/*
    Streamliner
    Created 2015 by Cesar Parra
    For RIT's HCI Capstone Project
    <http://www.cesareparra.com/>

    Licensed under The MIT Licenses
*/
define(function(require, exports, module) {

    var CommandManager = brackets.getModule("command/CommandManager"),
    Menus = brackets.getModule("command/Menus"),
    Dialogs = brackets.getModule("widgets/Dialogs"),
    DefaultDialogs = brackets.getModule("widgets/DefaultDialogs"),
    Document = brackets.getModule("document/Document"),
    EditorManager = brackets.getModule("editor/EditorManager"),
    WorkspaceManager = brackets.getModule("view/WorkspaceManager"),
    ExtensionUtils = brackets.getModule("utils/ExtensionUtils"),
    KeyBindingManager = brackets.getModule("command/KeyBindingManager"),
    CodeInspection = brackets.getModule("language/CodeInspection"), 
    PreferencesManager = brackets.getModule("preferences/PreferencesManager"),
    AppInit = brackets.getModule("utils/AppInit");

    //Load local modules
    var utils = require("src/utils/utils");
    var global = require("src/utils/global");
    var skeleton = require("src/skeleton");
    var cssreset = require("src/cssreset");
    var htmlvalidation = require("src/htmlvalidation");
    var cssvalidation = require("src/cssvalidation");
    var cssminifier = require("src/cssminifier");
    var typography = require("src/typography");
    var palette = require("src/palette");
    var codehinter = require("src/codehinter");
    var codehintercss = require("src/codehintercss");
    var usersettings = require("src/usersettings");
    var help = require("src/help");
    
    var prefs = PreferencesManager.getExtensionPrefs("streamliner");
    var stateManager = PreferencesManager.stateManager.getPrefixedSystem("streamliner");
    
    AppInit.appReady(function () {
        
        //TO DELETE WHEN DONE
        utils.log("Streamliner Extension Initializing");  
        
        ExtensionUtils.loadStyleSheet(module, "css/streamliner.css");
        ExtensionUtils.loadStyleSheet(module, "css/mdn.css");
        
        //Define User Preferences    
        if (!prefs.get("shareColors")) {
            prefs.definePreference("shareColors", "boolean", true);
        }
        if (!prefs.get("shareFonts")) {
            prefs.definePreference("shareFonts", "boolean", true);
        }
        
        //Skeleton
        skeleton.registerCommand();
        
        //CSS Reset
        cssreset.registerCommand();
        
        global.menu.addMenuDivider();
        
        //Typography Tool
        typography.registerCommand();
        
        //Palette Tool
        palette.registerCommand();
        
        global.menu.addMenuDivider();
        
        //Code Hinter
        codehinter.registerHTMLCodeHinter();
        codehintercss.registerCSSCodeHinter();
        
        global.menu.addMenuDivider();
        
        //HTML Validation
        htmlvalidation.registerCommand();
        
        //CSS Validation
        cssvalidation.registerCommand();
        
        //CSS Minify
        cssminifier.registerCommand();
        
        global.menu.addMenuDivider();
        
        //User Settings
        usersettings.registerCommand();
        
        global.menu.addMenuDivider();
        
        //Help
        help.registerCommand();
    });
});